class RenameEditorsToPosts < ActiveRecord::Migration[5.1]
  def change
     rename_table :editors, :posts
  end
end

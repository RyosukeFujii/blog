Rails.application.routes.draw do
  resources :posts
  devise_for :users
  resources :tags, only: [:index, :show]
  root to: 'static_pages#home'
  get '/about', to: 'static_pages#about'
  get '/list', to: 'static_pages#list'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Ckeditor::Engine => '/ckeditor'
end

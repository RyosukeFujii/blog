if (typeof(CKEDITOR) != 'undefined') {
  CKEDITOR.editorConfig = function (config) {
    config.extraPlugins = 'widget';
    config.extraPlugins = 'dialog';
    config.extraPlugins = 'eqneditor';
    config.contentsCss = 'assets/stylesheets/application.scss';
    config.contentsCss = 'assets/stylesheets/static_pages.scss';
    config.format_tags = "p;pre;h4;h3;h2;div";
    config.startupOutlineBlocks = true;
  }
}
;

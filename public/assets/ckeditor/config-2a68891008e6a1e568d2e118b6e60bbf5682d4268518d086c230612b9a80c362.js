CKEDITOR.editorConfig = function (config) {
  config.extraPlugins = 'codesnippet';
  config.extraPlugins = 'widget';
  config.extraPlugins = 'dialog';
  config.extraPlugins = 'eqneditor';
}

CKEDITOR.editorConfig = function( config ) {
  config.startupOutlineBlocks = true;
  config.format_tags = "p;pre;h4;h3;h2;div";
}


CKEDITOR.config.contentsCss = 'assets/stylesheets/static_pages.scss';
CKEDITOR.config.contentsCss = 'assets/stylesheets/application.scss';

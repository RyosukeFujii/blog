class ApplicationController < ActionController::Base
  before_action :detect_device
  protect_from_forgery with: :exception

  private
    def detect_device
      case request.user_agent
        #when /iPad/
        #    request.variant = :mobile
        when /iPod/
            request.variant = :mobile
        when /iPhone/
            request.variant = :mobile
        when /Android/
            request.variant = :mobile
      end
    end
end

class StaticPagesController < ApplicationController

  def home
    @lists = Post.page(params[:page]).reverse_order
  end

  def about
  end

  def list
    @lists = Post.page(params[:page]).reverse_order
  end

end
